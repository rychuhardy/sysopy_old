//
// Created by ry on 3/1/16.
//

#include "LinkedList.h"


/*
 * Node implementation.
 */
struct Node {
    value_t * contact;
    Node_t* next;
    Node_t* prev;
};

static int Node_create(Node_t** node, value_t * contact)
{

    *node = (Node_t*)malloc(sizeof(Node_t));

    if(!*node) {
        fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
        return BAD_ALLOC;
    }
    (*node)->contact=contact;
    (*node)->next=NULL;
    (*node)->prev=NULL;

    return 0;

}

value_t *Node_get(Node_t *node) {
    if(node) {
        return node->contact;
    }
    return NULL;
}


int Node_set(Node_t *node, value_t * contact) {
    if(node) {
        node->contact=contact;
        return 0;
    }
    return NULL_PTR;
}

value_t * Node_release(Node_t* node)
{

    if(node) {

        value_t *contact = node->contact;
        node->contact = NULL;

        return contact;
    }
    return NULL;

}

Node_t *Node_next(Node_t *node) {
    return node->next;
}

Node_t *Node_prev(Node_t *node) {
    return node->prev;
}

void Node_destroy(Node_t* node)
{

    if(node) {
        Contact_destroy(node->contact);
        free(node);
    }

}
/*
 * End of Node implementation.
 */


/*
 * Linked List implementation.
 */
struct LinkedList {
    Node_t* head;
    Node_t* tail;

    unsigned size_;

    int (*compare)(value_t * t1, value_t * t2);
};

int LinkedList_create(LinkedList_t** list) {

     *list = (LinkedList_t*)malloc(sizeof(LinkedList_t));

    if(!*list) {
        fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
        return BAD_ALLOC;
    }

    (*list)->size_ = 0;
    (*list)->compare = NULL;
    (*list)->head=NULL;
    (*list)->tail=NULL;

    return 0;
}


int LinkedList_size(LinkedList_t *list) {
    if(list) {
        return list->size_;
    }
    return NULL_PTR;
}

Node_t *LinkedList_get(LinkedList_t* list, int index) {

    if(list) {

        Node_t *ptr = list->head;
        int i = 0;
        while (i < index && ptr) {
            ptr = ptr->next;
            ++i;
        }
        return ptr;
    }
    return NULL;
}

Node_t *LinkedList_front(LinkedList_t *list) {

    if(list) {
        return list->head;
    }
    return NULL;
}

Node_t *LinkedList_back(LinkedList_t *list) {

    if(list) {
        return list->tail;
    }
    return NULL;
}

int LinkedList_push_front(LinkedList_t *list, value_t *value) {

    if(list) {

        Node_t* node;
        Node_create(&node, value);
        if(!node) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }
        node->contact=value;

        if(list->size_ == 0) {
            list->tail = node;
        }

        node->next = list->head;
        node->prev = NULL;

        if(list->head) {
            list->head->prev=node;
        }
        list->head=node;

        ++list->size_;
        return 0;
    }
    return NULL_PTR;

}

int LinkedList_push_back(LinkedList_t *list, value_t *value) {

    if(list) {

        Node_t* node;
        Node_create(&node, value);
        if(!node) {
            fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
            return BAD_ALLOC;
        }

        node->contact = value;

        node->next = NULL;
        node->prev = list->tail;

        if(list->size_ == 0) {
            list->head = node;
        }

        if(list->tail) {
            list->tail->next = node;
        }
        list->tail = node;

        ++list->size_;
        return 0;
    }
    return NULL_PTR;
}


int LinkedList_pop_front(LinkedList_t **list) {

    if(*list && (*list)->head) {
//        1 element list case
        if((*list)->size_ == 1) {
            (*list)->tail = NULL;
        }
//        normal case
        (*list)->head = (*list)->head->next;
        Node_destroy((*list)->head->prev);
        if((*list)->head) {
            (*list)->head->prev = NULL;
        }
        --(*list)->size_;
        return 0;
    }
    return NULL_PTR;

}

int LinkedList_pop_back(LinkedList_t **list) {

    if((*list) && (*list)->tail) {
//        1 element list case
        if((*list)->size_ == 1) {
            (*list)->head = NULL;
        }

//        normal case
        (*list)->tail = (*list)->tail->prev;
        Node_destroy((*list)->tail->next);
        if((*list)->tail) {
            (*list)->tail->next = NULL;
        }
        --(*list)->size_;
        return 0;
    }
    return NULL_PTR;

}

int LinkedList_insert(LinkedList_t *list, value_t *value, int index) {

    if(list) {


        if(index==0) {
            return LinkedList_push_front(list, value);

        }

        if(index == list->size_) {
            return LinkedList_push_back(list, value);

        }

        if(index < 0 || index >= list->size_) {
            fprintf(stderr, "range_error in: %s: line: %d", __FILE__, __LINE__);
            return RANGE_ERROR;
        }

        Node_t* iter = LinkedList_get(list, index);
            Node_t* node;
            Node_create(&node, NULL);
            if(!node) {
                fprintf(stderr, "bad_alloc in %s: line: %d", __FILE__, __LINE__);
                return BAD_ALLOC;
            }
        node->contact = value;

//            Previous element
            if(!iter->prev) {
                fprintf(stderr, "corrupted_list in %s: line: %d", __FILE__, __LINE__);
                return CORRUPTED_LIST;
            }
            node->prev = iter->prev;
            iter->prev->next = node;

//            Next element
            iter->prev = node;
            node->next = iter;

            ++list->size_;
            return 0;
    }
    return NULL_PTR;
}

int LinkedList_remove(LinkedList_t *list, value_t *value) {

    if(list) {
        Node_t* node = LinkedList_find(list, value);

        if(!node) {
            return NOT_FOUND;
        }

        if(node == list->head) {
            return LinkedList_pop_front(&list);
        }

        if(node == list->tail) {
            return LinkedList_pop_back(&list);
        }

        node->prev->next = node->next;
        node->next->prev = node->prev;


        Node_destroy(node);

        --list->size_;
        return 0;
    }
    return NULL_PTR;
}

Node_t *LinkedList_find(LinkedList_t *list, value_t *value) {

    if(list && list->size_ && list->compare) {
        Node_t* ptr = list->head;

        while(ptr && !list->compare(ptr->contact,value)) {
            ptr=ptr->next;
        }
        return ptr;
    }
    return NULL;
}

void LinkedList_set_comparator(LinkedList_t *list, int (*comp)(value_t *, value_t *)) {

    if(list && comp) {
        list->compare=comp;
    }
}


int LinkedList_join(LinkedList_t *l1, LinkedList_t *l2) {

    if(l1 && l2) {
        l1->tail->next = l2->head;
        if(l2->head) {
            l2->head->prev = l1->tail;
        }
        l1->tail = l2->tail;
        l1->size_ += l2->size_;

        l2->head = NULL;
        l2->tail = NULL;
        l2->size_ = 0;
        return 0;
    }
    return NULL_PTR;
}

static Node_t* LinkedList_merge(Node_t *n1, Node_t *n2, int (*comp)(Contact_t*, Contact_t*)) {

        Node_t* guard, *out;
        Node_create(&guard, NULL);
        out = guard;

        while(n1 && n2) {
            Node_t* current;
            if(comp(n1->contact,n2->contact) > 0) {
                current = n1;
                n1 = n1->next;
                current->next=NULL;
                n1->prev = NULL;
            }
            else {
                current = n2;
                n2 = n2->next;
                current->next = NULL;
                n2->prev = NULL;
            }
            out->next = current;
            current->prev = out;
            out = current;
        }
        if(n1) {
            out -> next = n1;
            n1->prev = out;
        }
        if(n2) {
            out -> next = n2;
            n2->prev = out;
        }
        n1 = guard->next;
        guard->next->prev = NULL;
        Node_destroy(guard);

        return n1;
//        LinkedList_destroy(l1);
//        LinkedList_destroy(l2);
    }

//Splits list without updating list's size. For internal use in sort only.
static void LinkedList_split(Node_t* iter) {
    if(iter->prev) {
        iter->prev->next = NULL;
        iter->prev = NULL;
    }
}
void LinkedList_advance(Node_t** t, int n)
{
    while(n>0 && *t) {
        (*t)=(*t)->next;
        --n;
    }
    while(n<0 && *t) {
        (*t)=(*t)->prev;
        ++n;
    }
}
static int cmp(const void* a, const void* b)
{
    Contact_t* aa = *(Contact_t**)a;
    Contact_t* bb = *(Contact_t**)b;

    return Contact_compare(aa, bb);
}

int LinkedList_sort(LinkedList_t *list) {
    if(list) {

        Contact_t** copy = malloc(sizeof(Contact_t*)*list->size_);

        int i=0;
        Node_t* ptr = list->head;
        while(ptr) {
            copy[i] = ptr->contact;
            ptr=ptr->next;
            ++i;
        }
//       for(int j=0; j<list->size_; ++j)
//           Contact_print(copy[j]);
        qsort(copy, list->size_, sizeof(copy[0]), cmp);

        ptr = list->head;
        i=0;
        while(ptr) {
            ptr->contact = copy[i];
            ptr=ptr->next;
            ++i;
        }

        free(copy);
        return 0;

    }
    return NULL_PTR;

}

void LinkedList_destroy(LinkedList_t *list) {

    if(list) {

        Node_t* ptr = list->head;
        Node_t* prev;
        while(ptr) {
            prev = ptr;
            ptr = Node_next(ptr);
            free(prev);
        }
    }
    free(list);

}

int LinkedList_foreach(LinkedList_t *list, void (*fun)(value_t *)) {

    if(list) {
        Node_t* ptr = list->head;
        while(ptr) {
            fun(ptr->contact);
            ptr=ptr->next;
        }
    }
    return NULL_PTR;

}

static void LinkedList_swap_value(Node_t* n1, Node_t* n2) {

    value_t * tmp = n1->contact;
    n1->contact = n2->contact;
    n2->contact = tmp;

}

/*
 * End of linked list implementation.
 */