//
// Created by ry on 3/1/16.
//

#ifndef LAB1_CONTACT_H
#define LAB1_CONTACT_H

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "ErrorCodes.h"

struct Contact;

typedef struct Contact Contact_t;

int Contact_create(Contact_t ** contact, const char* firstname, const char* lastname,
                   int year, int month, int day,
                   const char* email, const char* phone, const char* address);

//Contact_t* Contact_create(const char* firstname, const char* lastname,
//                   int year, int month, int day,
//                   const char* email, const char* phone, const char* address);


const char* Contact_get_firstname(Contact_t * contact);

const char* Contact_get_lastname(Contact_t * contact);

time_t Contact_get_birthdate(Contact_t * contact);

const char* Contact_get_email(Contact_t * contact);

const char* Contact_get_phone(Contact_t * contact);

const char* Contact_get_address(Contact_t * contact);

int Contact_set_firstname(Contact_t * contact, const char* firstname);

int Contact_set_lastname(Contact_t * contact, const char* lastname);

int Contact_set_birthdate(Contact_t * contact, int year, int month, int day);

int Contact_set_email(Contact_t * contact, const char* email);

int Contact_set_phone(Contact_t * contact, const char* phone);

int Contact_set_address(Contact_t * contact, const char* address);

void Contact_destroy(Contact_t * contact);

int Contact_compare(const Contact_t * c1, const Contact_t * c2);

int Contact_print(Contact_t * c);

#endif //LAB1_CONTACT_H
