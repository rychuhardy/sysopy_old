//
// Created by ry on 3/1/16.
//

#ifndef LAB1_LINKEDLIST_H
#define LAB1_LINKEDLIST_H


#include "Contact.h"

//Using typedef for better flexibility and reuse.
typedef Contact_t value_t;


/*
 * Linked List's node interface.
 */
struct Node;

typedef struct Node Node_t;

value_t * Node_get(Node_t* node);

int Node_set(Node_t* node, value_t * contact);

Node_t* Node_next(Node_t* node);

Node_t* Node_prev(Node_t* node);

value_t * Node_release(Node_t* node);

/*
 * Linked List interface.
 */
struct LinkedList;

typedef struct LinkedList LinkedList_t;

int LinkedList_create(LinkedList_t** list);

int LinkedList_size(LinkedList_t* list);

Node_t* LinkedList_get(LinkedList_t* list, int index);

Node_t* LinkedList_front(LinkedList_t* list);

Node_t* LinkedList_back(LinkedList_t* list);

int LinkedList_push_front(LinkedList_t* list, value_t * value);

int LinkedList_push_back(LinkedList_t* list, value_t * value);

int LinkedList_insert(LinkedList_t* list, value_t * value, int index);

int LinkedList_pop_front(LinkedList_t** list);

int LinkedList_pop_back(LinkedList_t** list);

int LinkedList_remove(LinkedList_t* list, value_t * value);

Node_t* LinkedList_find(LinkedList_t* list, value_t * value);

void LinkedList_set_comparator(LinkedList_t* list, int (*comp)(value_t *, value_t *));


void LinkedList_advance(Node_t** t, int n);
/*
 * Joins l2 to l1 and leaves l2 empty.
 */
int LinkedList_join(LinkedList_t* l1, LinkedList_t* l2);

int LinkedList_sort(LinkedList_t* list);

int LinkedList_foreach(LinkedList_t* list, void (*fun)(value_t * contact));

void LinkedList_destroy(LinkedList_t* list);

#endif //LAB1_LINKEDLIST_H
