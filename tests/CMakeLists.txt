cmake_minimum_required(VERSION 2.9)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11 -Wall -pedantic")


add_executable(test_static test_static.c test_cases.c test_util.c)
add_executable(test_shared test_shared.c test_cases.c test_util.c)
#add_executable(test_dynamic test_dynamic.c test_cases.c test_util.c)
add_executable(test_dynamic test_dynamic.c test_util.c)

target_link_libraries(test_static linkedlist_static)
target_link_libraries(test_shared linkedlist)
target_link_libraries(test_dynamic ${CMAKE_DL_LIBS})
